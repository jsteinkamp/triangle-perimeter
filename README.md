# Calculation of triangle perimeter

This repository contains a set of python3 functions, to calculate the center and radius a circle
of defined by three points and an example how it can be used to calculate and map the center of a
"private zone" as it can be defined in Strava, Garmin Connect and so on.

To create a map you need to copy the *data* folder and the 3 python scripts

* trianglePerimeter.py
* osm.py 
* mapping.py

Run the script by executing `python3 mapping.py`. You can use your own data, required are only
three points. Everything else is just accessory parts. 

The text in the doc folder can be found on my [Web page](https://www.j4s8.de/post/2018-10-21-triangle-perimeter/) page.


### Converting *doc/triangle-perimeter.md* to html
`pandoc -s --mathjax --mathml -o triangle-perimeter.html triangle-perimeter.md`
