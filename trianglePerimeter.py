#!/usr/bin/env python3

def sqsum(x):
    return(sum([y**2 for y in x]))

def trianglePerimeter(c):
    perim = {"center" : [-999.9, -999.9], "radius" : -999.9}

    if type(c) == 'numpy.ndarray':
        c = c.tolist()
    # TODO: check that c(coordinates) is a two dimensional tuple of
    # shape [x][2] with x >= 3
    s = [sqsum(c[i][:]) for i in range(len(c))]
    perim["center"][0] = (s[0] * (c[1][1] - c[2][1]) +
                          s[1] * (c[2][1] - c[0][1]) +
                          s[2] * (c[0][1] - c[1][1])) /\
                          (2 * (c[0][0] * (c[1][1] - c[2][1]) +
                          c[1][0] * (c[2][1] - c[0][1])+
                          c[2][0] * (c[0][1] - c[1][1])))
    perim["center"][1] = (s[1] - s[0] - 2 * perim["center"][0] * (c[1][0] - c[0][0])) /\
                         (2 * (c[1][1] - c[0][1]))
    perim["radius"] = (s[0] - 2 * perim["center"][0] * c[0][0] -
                      2 * perim["center"][1] * c[0][1] +
                      perim["center"][0]**2 +
                      perim["center"][1]**2)**0.5
    return(perim)

def testTrianglePerimeter():
    sqrt = 2**0.5
    coord = [[0, -2], [-sqrt, sqrt], [sqrt, sqrt]]
    print(trianglePerimeter(coord))
