#!/usr/bin/env python3

import json
from pyproj import Proj, transform
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

## local files
from trianglePerimeter import trianglePerimeter
from osm import getImageCluster

def listcolumns(l, n):
    return([l[i][n] for i in range(len(l))])

def getGeoPerimeter(cpts):
    ### Convert from geographic lon/lat to UTM projection
    mn = [sum(listcolumns(cpts, 0)) / len(cpts),
          sum(listcolumns(cpts, 1)) / len(cpts)] 
    utm = Proj(init="EPSG:32%i%02i" % (7 if mn[1] < 0 else 6, int(1 + (mn[0] + 180) / 6)))
    geo = Proj(init='epsg:4326')
    cpts = [transform(geo, utm, cpts[i][0], cpts[i][1]) for i in range(len(cpts))]

    ### Calculate the triangle Perimeter
    perim = trianglePerimeter(cpts[0:3])
    perim['center'][0], perim['center'][1] = transform(utm, geo, perim['center'][0], perim['center'][1])
    return(perim)

def getBBox(files):
    bbox=[999.9, -999.9, -999.9, 999.9]
    for file in files:
        with open(file) as fh:
            obj=json.load(fh)
            for f in obj['features']:
                if f['geometry']['type'] == 'Point':
                    #print('Point')
                    bbox[0] = min([bbox[0], f['geometry']['coordinates'][0]])
                    bbox[1] = max([bbox[1], f['geometry']['coordinates'][1]])
                    bbox[2] = max([bbox[2], f['geometry']['coordinates'][0]])
                    bbox[3] = min([bbox[3], f['geometry']['coordinates'][1]])
                elif f['geometry']['type'] == 'Polygon' or f['geometry']['type'] == 'MultiLineString':
                    #print(f['geometry']['type'])
                    crds=np.array(f['geometry']['coordinates'])
                    bbox[0] = min([bbox[0], crds[0,:,0].min()]) # lon
                    bbox[1] = max([bbox[1], crds[0,:,1].max()]) # lat
                    bbox[2] = max([bbox[2], crds[0,:,0].max()]) # lon
                    bbox[3] = min([bbox[3], crds[0,:,1].min()]) # lat
                else:
                    print(f['geometry']['type'])
    return(bbox)

bbox = getBBox(glob("data/*.geojson"))
#print(bbox)
#print(bbox[0], bbox[3], bbox[2] - bbox[0],  bbox[1] - bbox[3])

map = getImageCluster(bbox[0], bbox[3], bbox[2] - bbox[0],  bbox[1] - bbox[3], 14)
fig, axs = plt.subplots(1, 1, figsize=(13, 9), dpi=96)
axs.imshow(np.asarray(map['image']),
           interpolation="bilinear",
           extent=[map['bbox'][0][0],
                   map['bbox'][1][0],
                   map['bbox'][1][1],
                   map['bbox'][0][1]])

with open("data/circle.geojson", "rt") as jsonin:
    circle = json.load(jsonin)
    circle = np.array(circle['features'][0]['geometry']['coordinates'])
    plt.fill(circle[0,:,0], circle[0,:,1], color='#AA000055')
with open("data/running.geojson", "rt") as jsonin:
    running = json.load(jsonin)
    running = np.array(running['features'][0]['geometry']['coordinates'])
    plt.plot(running[0,:,0], running[0,:,1], color='blue')
with open("data/cycling.geojson", "rt") as jsonin:
    cycling = json.load(jsonin)
    cycling = np.array(cycling['features'][0]['geometry']['coordinates'])
    plt.plot(cycling[0,:,0], cycling[0,:,1], color='green')
with open("data/home.geojson", "rt") as jsonin:
    home = json.load(jsonin)
    plt.plot(home['features'][0]['geometry']['coordinates'][0],
             home['features'][0]['geometry']['coordinates'][1], 'bx')
cp =[]
with open("data/crosspoints.geojson", "rt") as jsonin:
    crosspoints = json.load(jsonin)
    for feature in crosspoints['features']:
        cp.append(feature['geometry']['coordinates'])
        plt.plot(feature['geometry']['coordinates'][0],
                 feature['geometry']['coordinates'][1], 'rx')

res = getGeoPerimeter(cp)
print(res)
plt.plot(res['center'][0],
         res['center'][1], 'rx')

fig.savefig("map.png")
