% Triangle Perimeter
% Joerg Steinkamp
% October 21, 2018

A circle is through the origin is defined as:

\begin{equation}
    x^2 + y^2 = r^2
\end{equation}

The circle can be moved to a new center $x_m$/$y_m$:

\begin{equation}
    (x - x_m)^2 + (y - y_m)^2 = r^2
\end{equation}

Reordering this equation gives:

\begin{equation}
    x^2 + y^2 = 2 x_m x + 2 y_m y - y_m^2 - x_m^2 + r^2
\end{equation}

1. With $Q = r^2 - y_m^2 - x_m^2$ and $S_i = x_i^2 + y_i^2$ and three points $x_{1-3}/y_{1-3}$ I
get three equations with three unknowns ($x_m$, $y_m$, $Q$):

\begin{align}
    S_1 &=& 2 x_m x_1 + 2 y_m y_1 + Q\label{eq:1a}\\
    S_2 &=& 2 x_m x_2 + 2 y_m y_2 + Q\\
    S_3 &=& 2 x_m x_3 + 2 y_m y_3 + Q
\end{align}

2. Subtracting equation [1a](#eq:1a) from the two others to eliminate Q

\begin{align}
    S_1       &=& 2 x_m x_1 + 2 y_m y_1 + Q\\
    S_2 - S_1 &=& 2 x_m (x_2 - x_1) + 2 y_m (y_2 - y_1) \label{eq:2b} \\
    S_3 - S_1 &=& 2 x_m (x_3 - x_1) + 2 y_m (y_3 - y_1) \label{eq:2c}
\end{align}

3. Multiplying [2b](#eq:2b) by $(y_3 - y_1)$ and [2c](#eq:2c) by $(y_2 - y_1)$

\begin{align}
    S_1                     &=& 2 x_m x_1 + 2 y_m y_1 + Q\\
    (S_2 - S_1) (y_3 - y_1) &=& 2 x_m (x_2 - x_1) (y_3 - y_1) + 2 y_m (y_2 - y_1) (y_3 - y_1) \label{eq:3b} \\
    (S_3 - S_1) (y_2 - y_1) &=& 2 x_m (x_3 - x_1) (y_2 - y_1) + 2 y_m (y_3 - y_1) (y_2 - y_1) \label{eq:3c}
\end{align}


4. Subtracting [3b](#eq:3b) from [3c](#eq:3c) and simplifying

\begin{align}
    S_1 &=& 2 x_m x_1 + 2 y_m y_1 + Q\\
    (S_2 - S_1) (y_3 - y_1) &=& 2 x_m (x_2 - x_1) (y_3 - y_1) + 2 y_m (y_2 - y_1) (y_3 - y_1) \label{eq:4b} \\
    S_1 (y_2 - y_3) + S_2 (y_3 - y_1) + S_3 (y_1 - y_2) &=& 2 x_m (x_1 (y_2 - y_3) + x_2 (y_3 - y_1) + x_3 (y_1 - y_2)) \label{eq:4c}
\end{align}

5. Equation [4c](#eq:4c) now contains only the unknown $x_m$:

\begin{equation}\label{eq:5}
    x_m = \frac{S_1 (y_2 - y_3) + S_2 (y_3 - y_1) + S_3 (y_1 - y_2)}{2(x_1 (y_2 - y_3) + x_2 (y_3 - y_1) + x_3 (y_1 - y_2))}
\end{equation}

6. Equation [2b](#eq:2b)contains the unknowns $y_m$ and $x_m$ and with $x_m$ from [5](#eq:5)

\begin{equation}\label{eq:6}
    y_m = \frac{S_2 - S_1 - 2 x_m (x_2 - x_1)}{2 (y_2 - y_1)}
\end{equation}

7. Solving [1a](#eq:1a) and with $Q =  r^2 - y_m^2 - x_m^2$ gives

\begin{equation}\label{eq:7}
     r = \sqrt{S_1 - 2 x_m x_1 - 2y_m y_1  + y_m^2 + x_m^2}
\end{equation}

An similar example with numbers (in german only) can be found here: [Kreis durch drei
Punkte](http://www.arndt-bruenner.de/mathe/scripts/kreis3p.htm). One can also solve it
geometrically, by drawing orthogonal lines through the center of each triangle side.The center
of the perimeter is,  where these lines intersect.
