import math
import random
from urllib.request import urlopen
from io import BytesIO
from PIL import Image

def deg2num(lon_deg, lat_deg, zoom):
  lat_rad = math.radians(lat_deg)
  n = 2.0 ** zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
  return(xtile, ytile)

## NW corner
## https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Tile_numbers_to_lon..2Flat._2
def num2deg(xtile, ytile, zoom):
  n = 2.0 ** zoom
  lon_deg = xtile / n * 360.0 - 180.0
  lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
  lat_deg = math.degrees(lat_rad)
  return([lon_deg, lat_deg])

def getImageCluster(lon_deg, lat_deg,  delta_lon, delta_lat, zoom):
  smurl = r"http://{0}.tile.openstreetmap.org/{1}/{2}/{3}.png"
  xmin, ymax = deg2num(lon_deg, lat_deg, zoom)
  xmax, ymin = deg2num(lon_deg + delta_lon, lat_deg + delta_lat, zoom)
  #print([xmin, ymax, xmax, ymin])
  bbox = [num2deg(xmin, ymin, zoom), num2deg(xmax+1, ymax+1, zoom)]

  img = Image.new('RGB',((xmax-xmin+1)*256-1,(ymax-ymin+1)*256-1) ) 
  for xtile in range(xmin, xmax+1):
    for ytile in range(ymin,  ymax+1):
      #print(num2deg(xtile, ytile, zoom))
      #print(num2deg(xtile+1, ytile+1, zoom))
      try:
        imgurl = smurl.format(random.choice(["a", "b", "c"]), zoom, xtile, ytile)
        #print("Opening: " + imgurl)
        imgstr = urlopen(imgurl).read()
        tile = Image.open(BytesIO(imgstr))
        img.paste(tile, box=((xtile-xmin)*256 ,  (ytile-ymin)*256))
      except: 
        print("Couldn't download image")
        tile = None
  return({'bbox': bbox, 'image': img})
